import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeaturesRoutingModule } from './features-routing.module';
import {ContainerModule} from '@features/container/container.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ContainerModule,
    FeaturesRoutingModule
  ]
})
export class FeaturesModule { }
