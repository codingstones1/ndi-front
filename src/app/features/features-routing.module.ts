import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardHabitatComponent} from "@features/container/habitat/dashboard-habitat/dashboard-habitat.component";
import {HabitatMeteoComponent} from "@features/container/habitat/habitat-meteo/habitat-meteo.component";
import {HabitatCarteComponent} from "@features/container/habitat/habitat-carte/habitat-carte.component";
import {HabitatEnergyComponent} from "@features/container/habitat/habitat-energy/habitat-energy.component";
import {HabitatLogsComponent} from "@features/container/habitat/habitat-logs/habitat-logs.component";
import {ErrorComponent} from "@domains/error/error.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: '/habitat',
    pathMatch: 'full'
  },
  {
    path: 'habitat',
    component: DashboardHabitatComponent,
  },
  {
    path: 'habitat/meteo',
    component: HabitatMeteoComponent
  },
  {
    path: 'habitat/carte',
    component: HabitatCarteComponent
  },
  {
    path: 'habitat/energy',
    component: HabitatEnergyComponent
  },
  {
    path: 'habitat/logs',
    component: HabitatLogsComponent
  },
  {
    path: '**',
    component: ErrorComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
