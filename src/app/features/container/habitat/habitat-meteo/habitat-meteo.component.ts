import {Component, OnInit} from '@angular/core';
import {MeteoService} from "@services/meteo.service";
import {Hygrometry, Pression, Surface, Temperature, Wind} from '@core/models/Meteo';

@Component({
  selector: 'app-habitat-meteo',
  templateUrl: './habitat-meteo.component.html',
  styleUrls: ['./habitat-meteo.component.scss']
})
export class HabitatMeteoComponent implements OnInit {

  private _instantTemperatureSurface: Surface;
  private _instantPression: Pression;
  private _instantWind: Wind;
  _instantHygrometrie: Hygrometry[];

  private _temperatures: Temperature;
  private _pression: Pression[];
  private _wind: Wind[];
  temp = '0';
  pres = '0';
  wind = '0';
  hydro = '0';

  constructor(
    private meteoService: MeteoService
  ) {
    this.meteoService.getTemp().subscribe(data => {
      this._temperatures = data;
      this.boucleTemp();
    });

    this.meteoService.getPression().subscribe(data => {
      this._pression = data;
      this.bouclePres();
    });

    this.meteoService.getWind().subscribe(data => {
        this._wind = data;
        this.boucleWind();
      });

    this.meteoService.getHygro().subscribe(data => {
      this._instantHygrometrie = data;
      this.boucleHygro();
    });
  }

  ngOnInit() {

  }

  boucleTemp() {
    let i = 0;
    const max = this._temperatures.surface.length;
    setInterval(() => {
      if (i < max) {
        this.temp = this._temperatures.surface[i].value.toFixed(2);
        i++;
      } else {
        this.boucleTemp();
      }
    }, 1000);

  }

  bouclePres(){
    let i = 0;
    const max = this._pression.length;
    setInterval(() => {
      if (i < max) {
        this.pres = this._pression[i].value.toFixed(2);
        i++;
      } else {
        this.boucleTemp();
      }
    }, 1000);

  }

  boucleHygro() {
    let i = 0;
    const max = this._instantHygrometrie.length;
    setInterval(() => {
      if (i < max) {
        this.hydro = this._instantHygrometrie[i].value.toFixed(2);
        i++;
      } else {
        this.boucleHygro();
      }
    }, 1000);

  }

  boucleWind() {
    let i = 0;
    const max = this._wind.length;
    setInterval(() => {
      if (i < max) {
        this.wind = this._wind[i].value.toFixed(2);
        i++;
      } else {
        this.boucleWind();
      }
    }, 1000);

  }

  get instantTemperatureSurface() {
    return this._instantTemperatureSurface;
  }

  get hygrometrie() {
    return this._instantHygrometrie;
  }

  get pression() {
    return this._instantPression;
  }


}
