import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HabitatMeteoComponent } from './habitat-meteo.component';

describe('HabitatMeteoComponent', () => {
  let component: HabitatMeteoComponent;
  let fixture: ComponentFixture<HabitatMeteoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitatMeteoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HabitatMeteoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
