import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HabitatEnergyComponent } from './habitat-energy.component';

describe('HabitatEnergyComponent', () => {
  let component: HabitatEnergyComponent;
  let fixture: ComponentFixture<HabitatEnergyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitatEnergyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HabitatEnergyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
