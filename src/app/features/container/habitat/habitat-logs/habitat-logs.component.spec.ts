import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HabitatLogsComponent } from './habitat-logs.component';

describe('HabitatLogsComponent', () => {
  let component: HabitatLogsComponent;
  let fixture: ComponentFixture<HabitatLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitatLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HabitatLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
