import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HabitatCarteComponent } from './habitat-carte.component';

describe('HabitatCarteComponent', () => {
  let component: HabitatCarteComponent;
  let fixture: ComponentFixture<HabitatCarteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitatCarteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HabitatCarteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
