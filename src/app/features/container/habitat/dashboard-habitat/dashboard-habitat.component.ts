import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-habitat',
  templateUrl: './dashboard-habitat.component.html',
  styleUrls: ['./dashboard-habitat.component.scss']
})
export class DashboardHabitatComponent implements OnInit {



  constructor() { }

  ngOnInit() {

  }

  toggleClass() {
    const elem = document.getElementById('html');
    if (elem.classList.contains('nocturne')) {
      elem.classList.remove('nocturne');
    } else {
      elem.classList.add('nocturne');
    }
  }


}
