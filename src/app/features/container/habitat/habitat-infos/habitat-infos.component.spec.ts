import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HabitatInfosComponent } from './habitat-infos.component';

describe('HabitatInfosComponent', () => {
  let component: HabitatInfosComponent;
  let fixture: ComponentFixture<HabitatInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitatInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HabitatInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
