import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardHabitatComponent} from '@features/container/habitat/dashboard-habitat/dashboard-habitat.component';
import {HabitatCarteComponent} from '@features/container/habitat/habitat-carte/habitat-carte.component';
import {HabitatEnergyComponent} from '@features/container/habitat/habitat-energy/habitat-energy.component';
import {HabitatLogsComponent} from '@features/container/habitat/habitat-logs/habitat-logs.component';
import {HabitatMeteoComponent} from '@features/container/habitat/habitat-meteo/habitat-meteo.component';
import {HabitatInfosComponent} from '@features/container/habitat/habitat-infos/habitat-infos.component';
import { FormsModule } from "@angular/forms";


@NgModule({
  declarations: [
    DashboardHabitatComponent,
    HabitatCarteComponent,
    HabitatEnergyComponent,
    HabitatLogsComponent,
    HabitatMeteoComponent,
    HabitatInfosComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    DashboardHabitatComponent,
    HabitatCarteComponent,
    HabitatEnergyComponent,
    HabitatLogsComponent,
    HabitatMeteoComponent,
    HabitatInfosComponent
  ]
})
export class HabitatModule { }
