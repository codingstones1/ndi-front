import { Injectable } from '@angular/core';
import {environment} from '@env/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';


function ObjectToString(data: Object): {[key: string]: string} {
  const res = {};
  for (const k in data) {
    const v = data[k];
    res[k] = typeof v === 'string' ? v : JSON.stringify(v);
  }
  return res;
}

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private _http: HttpClient) { }

  private async get<T>(url: string, data: Object): Promise<HttpResponse<T>> {
    return this._http.get<T>( url, {
      observe: 'response',
      params: {...ObjectToString(data)}
    }).toPromise();
  }


  async getData(): Promise<string> {
    const url = `${environment.back.url}`;
    const res = await this.get<{data: string}>(url,  {});
    return res.body.data;
  }
}
