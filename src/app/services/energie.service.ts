import {Meteo, Pression, Surface, Temperature, Values, Wind} from "@core/models/Meteo";
import {HttpClient} from "@angular/common/http";
import {environment} from "@env/environment";
import {Observable} from "rxjs/internal/Observable";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class EnergieService {

  constructor(private _http: HttpClient) { }

  getEoliennePower() : Observable<Surface> {
    return this._http.get( `${environment.back.url}/meteo/temperature/surface/last`);
  }


}
