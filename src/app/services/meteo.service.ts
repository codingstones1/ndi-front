import {Hygrometry, Meteo, Pression, Surface, Temperature, Values, Wind} from "@core/models/Meteo";
import {HttpClient} from "@angular/common/http";
import {environment} from "@env/environment";
import {Observable} from "rxjs/internal/Observable";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})

export class MeteoService {

  constructor(private _http: HttpClient) { }

  getTemp(): Observable<Temperature> {
    return this._http.get<Temperature>(`${environment.back.url}/meteo/temperatures`);
  }

  getPression(): Observable<Pression[]> {
    return this._http.get<Pression[]>( `${environment.back.url}/meteo/pressions`);
  }

  getWind(): Observable<Wind[]>{
    return this._http.get<Wind[]>( `${environment.back.url}/meteo/winds`);
  }


  getHygro(): Observable<Hygrometry[]>{
    return this._http.get<Hygrometry[]>( `${environment.back.url}/meteo/hygrometries`);
  }
}
