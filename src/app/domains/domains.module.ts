import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ErrorComponent} from "@domains/error/error.component";

@NgModule({
  declarations: [
    ErrorComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DomainsModule { }
