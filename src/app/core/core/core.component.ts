import { Component, OnInit } from '@angular/core';
import {TestService} from '@services/test.service';

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent implements OnInit {

  readValue: string;

  constructor(private testService: TestService) {
    this.testService.getData().then( d =>
      this.readValue = d
    );
  }

  ngOnInit() {
  }

}
