export interface Temperature {
  surface?: Surface[];
}
export interface Surface {
  date?: string;
  value?: number;
}
export interface Oxygen {
  values?: Values[];
}
export interface Values {
  date?: string;
  value?: number;
}
export interface Pression {
  type?: string;
  value?: number;
}
export interface Humidity {
  values?: Values[];
}
export interface Position {
  type?: string;
  values?: PositionValues[];
}
export interface PositionValues {
  date?: string;
  altitude?: number;
  longitude?: number;
}
export interface Wind {
  surface? : Surface[];
  value?: number;
}
export interface Hygrometry {
  date?: string;
  value?: number;
}
export interface Day {
  values: DayValues[];
}
export interface DayValues {
  date?: string;
  sunrise?: string;
  sunset?: string;
  wind?: number;
  humidity?: number;
  precipitation?: number;
  temperature?: number;
}
export interface Robot {
  values?: RobotValues[];
}
export interface RobotValues {
  date?: string;
  battery?: number;
  position?: {
    altitude?: number;
    longitude?: number;
  };
  etat?: string;
  task_id?: string;
}

export interface Meteo {
  temperature?: Temperature;
  oxygen?: Oxygen;
  pression?: Pression;
  humidity?: Humidity;
  position?: Position;
  wind?: Wind;
  hygrometry?: Hygrometry;
  day?: Day;
  robot?: Robot;
  task?: {
    values?: string;
  }
}
