import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core/core.component';
import {BrowserModule} from '@angular/platform-browser';
import {FeaturesModule} from '@features/features.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";

@NgModule({
  declarations: [
    CoreComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FeaturesModule,
    HttpClientModule,
    RouterModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot()
  ],
  bootstrap: [
    CoreComponent
  ],
  exports: [
    CoreComponent
  ]
})
export class CoreModule { }
